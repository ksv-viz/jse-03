# Task manager:
* Управление списком задач

# Software:
Product | Version
--- | ---
*java* | `11`
*maven* | `3.6`

# Developer
 serg.v.kazakov
 * serg.v.kazakov@mail.ru

# Build:
```
 mvn clean package install 
```

# Run:
```
 java -jar task-manager-1.0.0.jar
```
